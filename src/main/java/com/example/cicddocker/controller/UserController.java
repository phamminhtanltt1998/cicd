package com.example.cicddocker.controller;

import com.example.cicddocker.entity.User;
import com.example.cicddocker.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {
    @Autowired
    private UserRepo userRepo ;

    @GetMapping
    public List<User> getListUsers(){
       return  userRepo.findAll();
    }
    @PostMapping
    public User addUser(@RequestBody User user){
       return  userRepo.save(user);
    }
}
